from winpcapy import *
import sys

alldevs=POINTER(pcap_if_t)() 
pcap_errbuf = create_string_buffer(PCAP_ERRBUF_SIZE)

if (pcap_findalldevs(byref(alldevs), pcap_errbuf) == -1):
    print "No interfaces found"
    sys.exit(1)

available_interface = alldevs.contents
while available_interface:
    devname = '{' + available_interface.name.split('{')[1]
    print devname
    available_interface = available_interface.next.contents

#fp = pcap_open_live(available_interface.contents.name,65536,1,10,pcap_errbuf)
if fp is None:
    print "Error opening device"
    sys.exit(1)


#pcap_return = pcap_sendpacket(fp,packet,writeLen+14)
