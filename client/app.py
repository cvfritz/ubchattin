#!/usr/bin/env python
#Christopher Fritz, EE513, Spring 2015
#Audio transcriber host - receives data over xbee and converts to text, in wx application.

import wx
import glob
import thread
import time
from scipy import signal
from numpy import *
import serial
import speech_recognition as sr
import os

SAMPLERATE = 2500
THRESHOLD = 500
MEAN_VALUE = 400
MIN_COUNT = 2500 #For now, wait for silence of 1 sec. 

class SoundFile:
   def  __init__(self, signal):
       self.file = wave.open('low_rate_audio.wav', 'wb')
       self.signal = signal
       self.sr = SAMPLERATE

   def write(self):
       self.file.setparams((1, 2, self.sr, duration*SAMPLERATE, 'NONE', 'noncompressed'))
       self.file.writeframes(self.signal)
       self.file.close()


class MainFrame(wx.Frame):
    def __init__(self, parent):
        wx.Frame.__init__(self, parent, size=(450,600), title="EE513 - Voice Data Transcription Host")
        panel = wx.Panel(self)
        self.quote = wx.StaticText(panel, label="XBEE Rx Serial Port: ", pos=(20, 15))
        self.serialCombo = wx.ComboBox(panel, pos=(160,10), choices=glob.glob('/dev/tty.*'))
        

        self.startButton = wx.ToggleButton(panel, label="Start Transcription ", pos=(150, 45))
        self.startButton.Bind(wx.EVT_TOGGLEBUTTON, self.onStartButton)
        self.lab1 = wx.StaticText(panel, label="Voice log:", pos=(20, 80))
        self.transcribed = wx.TextCtrl(panel, style=wx.TE_MULTILINE, pos=(10, 100), size=(420, 100))

        self.lab2 = wx.StaticText(panel, label="Output file directory:", pos=(10, 215))
        self.outfile = wx.DirPickerCtrl(panel, pos=(145, 208), path=os.getcwd())

        self.box1 = wx.StaticBox(panel, pos=())

        self.kill = False
        self.Show()

    def onStartButton(self, event):
        if "Start" in self.startButton.GetLabel():
            self.startButton.SetLabel("Stop Transcription")
            self.kill = False
            thread.start_new_thread(self.sampleThread, ())
        else:
            self.startButton.SetLabel("Start Transcription")
            self.kill = True

    def sampleThread(self):
        s = serial.Serial(self.serialCombo.GetValue(), baudrate=115200)

        while not self.kill:
            data = []
            seenThreshold = False
            thresholdCount = 0
            while True:
                se = s.read(2)
                xnew = (ord(se[1])<<8) | ord(se[0])
                if xnew > 1024:
                    s.read(1)
                    continue
                data.append(xnew)
                pt = abs(xnew - MEAN_VALUE) #Center around 0

                #The idea is to slice up the input data around intervals 
                #based on thresholding.
                if not seenThreshold:
                    if pt > THRESHOLD:
                        if thresholdCount < MIN_COUNT:
                            thresholdCount += 1
                        else:
                            seenThreshold = True
                            thresholdCount = 0
                    else:
                        thresholdCount = 0
                else:
                    if pt < THRESHOLD:
                        if thresholdCount < MIN_COUNT:
                            thresholdCount += 1
                        else:
                            break
                    else:
                        thresholdCount = 0

            data = array(data)            
            v = N.average(data)
            data -= av #Remove DC offset
            ydata = data * int(32768. / av)
            samples = SAMPLERATE * duration


            signal = N.resize(ydata, (samples,))

            ssignal = ''
            for i in range(len(signal)):
                if signal[i] > 32767:
                    signal[i] = 32767
                if signal[i] < -32767:
                    signal[i] = -32767
                ssignal += wave.struct.pack('h',signal[i]) # transform to binary

            f = SoundFile(ssignal)
            f.write()

            os.system("sh rateconv.sh")

            r = sr.Recognizer()
            with sr.WavFile("hi_rate_audio.wav") as source:     
                audio = r.record(source)                        
            try:
                tst = r.recognize(audio)   # recognize speech using Google Speech Recognition
            except LookupError:                                 # speech is unintelligible
                tst = ''

            tstr = time.strftime("%H:%M:%S") + "> " + tst + "\n"
            self.transcribed.SetValue(self.transcribed.GetValue() + tstr)

        s.close()
        

app = wx.App(False)
MainFrame(None)
app.MainLoop()
