import matplotlib.pyplot as plt
import serial
import time
import numpy as N
import wave
from scipy import signal
from numpy import *

SAMPLERATE = 2500
duration = 5
THRESHOLD = 420
PERSISTENCE = 500

s = serial.Serial('/dev/tty.usbserial-DA01LE4W', baudrate=115200)
#s = serial.Serial('/dev/tty.usbmodem1411', baudrate=115200)

filter_hz = 10

b, a = signal.butter(5, filter_hz / (SAMPLERATE/2.), btype='high')


_data = empty(duration*SAMPLERATE)

ind = 0
threshold_count = 0
print "Opened port"
while ind < duration*SAMPLERATE:
  se = s.read(2)
  #ar = [0|ord(se[x]) for x in arange(0,100,2)]
  #_data[50*ind:50*(ind+1)] = ar
  xnew = (ord(se[1])<<8) | ord(se[0])
  if xnew > 1024:
  	s.read(1)
  else:
	  _data[ind] = xnew
	  ind += 1
	  if xnew < THRESHOLD:
	  	threshold_count += 1
	  else:
	  	threshold_count = 0
s.close()


#avgval = _data[:ind].mean()
#_data[ind:] = avgval

#_data = signal.lfilter(b, a, _data)
		




#f = signal.firwin(63, 0.44)
#newsig = signal.convolve(_data, f)

plt.plot(_data)
#plt.plot(newsig)
plt.show()

#_data = newsig[31:-32]


#_data = newsig[:len(_data)]


class SoundFile:
   def  __init__(self, signal):
       self.file = wave.open('low_rate_audio.wav', 'wb')
       self.signal = signal
       self.sr = SAMPLERATE

   def write(self):
       self.file.setparams((1, 2, self.sr, duration*SAMPLERATE, 'NONE', 'noncompressed'))
       self.file.writeframes(self.signal)
       self.file.close()

# let's prepare signal
'''
duration = 4 # seconds
samplerate = 44100 # Hz
samples = duration*samplerate
frequency = 440 # Hz
period = samplerate / float(frequency) # in sample points
omega = N.pi * 2 / period

xaxis = N.arange(int(period),dtype = N.float) * omega
ydata = 16384 * N.sin(xaxis)
'''

#_data = N.array(_data)
av = N.average(_data)
_data -= av #Remove DC offset
ydata = _data * int(32768. / av)
samples = SAMPLERATE * duration


signal = N.resize(ydata, (samples,))

ssignal = ''
for i in range(len(signal)):
   if signal[i] > 32767:
      signal[i] = 32767
   if signal[i] < -32767:
      signal[i] = -32767
   ssignal += wave.struct.pack('h',signal[i]) # transform to binary

f = SoundFile(ssignal)
f.write()
print 'file written'

plt.plot(ydata)
plt.show()

