import speech_recognition as sr
import os

os.system("sh rateconv.sh")

r = sr.Recognizer()
with sr.WavFile("hi_rate_audio.wav") as source:     # use "test.wav" as the audio source
    audio = r.record(source)                        # extract audio data from the file

try:
    print("Transcription: " + r.recognize(audio))   # recognize speech using Google Speech Recognition
except LookupError:                                 # speech is unintelligible
    print("Could not understand audio")