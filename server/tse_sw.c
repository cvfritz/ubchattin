#include <altera_avalon_sgdma.h>
#include <altera_avalon_sgdma_descriptor.h>//include the sgdma descriptor
#include <altera_avalon_sgdma_regs.h>//include the sgdma registers
#include <altera_avalon_pio_regs.h> //include the PIO registers
#include <unistd.h>
#include <stdio.h>

#include "sys/alt_stdio.h"
#include "sys/alt_irq.h"
#include "system.h"


// Function Prototypes
void rx_ethernet_isr (void *context);
void tx_ethernet(unsigned char *frame, unsigned char frame_len);

// Create a receive frame
unsigned char rx_frame[1024] = { 0 };

unsigned char address_table[20][6];

unsigned char client_table[20][20];

unsigned char num_clients = 1;

int MAC[6] = {0x00, 0x1C, 0x23, 0x17, 0x4A, 0xCB};

//Other variables
int in=0, print;

// Create sgdma transmit and receive devices
alt_sgdma_dev * sgdma_tx_dev;
alt_sgdma_dev * sgdma_rx_dev;

// Allocate descriptors in the descriptor_memory (onchip memory)
alt_sgdma_descriptor tx_descriptor		__attribute__ (( section ( ".descriptor_memory" )));
alt_sgdma_descriptor tx_descriptor_end	__attribute__ (( section ( ".descriptor_memory" )));

alt_sgdma_descriptor rx_descriptor  	__attribute__ (( section ( ".descriptor_memory" )));
alt_sgdma_descriptor rx_descriptor_end  __attribute__ (( section ( ".descriptor_memory" )));


/********************************************************************************
 * This program demonstrates use of the Ethernet in the DE2i-150 board.
********************************************************************************/
int main(void){
	int i;
	// Open the sgdma transmit device
	sgdma_tx_dev = alt_avalon_sgdma_open ("/dev/sgdma_tx");
	if (sgdma_tx_dev == NULL) {
		alt_printf ("Error: could not open scatter-gather dma transmit device\n");
		//return -1;
	} else alt_printf ("Opened scatter-gather dma transmit device\n");

	// Open the sgdma receive device
	sgdma_rx_dev = alt_avalon_sgdma_open ("/dev/sgdma_rx");
	if (sgdma_rx_dev == NULL) {
		alt_printf ("Error: could not open scatter-gather dma receive device\n");
		//return -1;
	} else alt_printf ("Opened scatter-gather dma receive device\n");



	// Set interrupts for the sgdma receive device
	alt_avalon_sgdma_register_callback( sgdma_rx_dev, (alt_avalon_sgdma_callback) rx_ethernet_isr, 0x00000014, NULL );

	// Create sgdma receive descriptor
	alt_avalon_sgdma_construct_stream_to_mem_desc( &rx_descriptor, &rx_descriptor_end, (alt_u32 *)rx_frame, 0, 0 );

	// Set up non-blocking transfer of sgdma receive descriptor
	alt_avalon_sgdma_do_async_transfer( sgdma_rx_dev, &rx_descriptor );










	// Triple-speed Ethernet MegaCore base address
	volatile int * tse = (int *) TSE_BASE;

	// Specify the addresses of the PHY devices to be accessed through MDIO interface
	*(tse + 0x0F) = 0x10;

	// Disable read and write transfers and wait
	*(tse + 0x02) = *(tse + 0x02) | 0x00800220;
	while ( *(tse + 0x02) != ( *(tse + 0x02) | 0x00800220 ) );


	//MAC FIFO Configuration
	*(tse + 0x09) = TSE_TRANSMIT_FIFO_DEPTH-16;
	*(tse + 0x0E ) = 3;
	*(tse + 0x0D ) = 8;
	*(tse + 0x07) =TSE_RECEIVE_FIFO_DEPTH-16;
	*(tse + 0x0C ) = 8;
	*(tse + 0x0B ) = 8;
	*(tse + 0x0A ) = 0;
	*(tse + 0x08 ) = 0;

	// Initialize the MAC address
	*(tse + 0x03) = 0x17231C00;
	*(tse + 0x04) = 0x0000CB4A;

	// MAC function configuration
	*(tse + 0x05) = 1518;
	*(tse + 0x17) = 12;
	*(tse + 0x06) = 0xFFFF;
	*(tse + 0x02) = 0x00800220;


	// Software reset the PHY chip and wait
	*(tse + 0x02) =  0x00802220;
	while ( *(tse + 0x02) != ( 0x00800220 ) );

	// Enable read and write transfers, gigabit Ethernet operation and promiscuous mode
	
	*(tse + 0x02) = *(tse + 0x02) | 0x0080023B;

	while ( *(tse + 0x02) != ( *(tse + 0x02) | 0x0080023B ) );


	while (1) {
		print=in;
		in = IORD_ALTERA_AVALON_PIO_DATA(SWITCH_BASE)&0x01; //read the input from the switch

		 //switch on or switch off the LED

		if (in==1){
			if (print != in){
				IOWR_ALTERA_AVALON_PIO_DATA(LED_BASE, 1);
				alt_printf( "Switch on LED \n" );
				unsigned char frame[255] = {0};
				for (i=2; i < 8; i++) frame[i] = 0xFF;
				for (i=8; i < 14; i++) frame[i] = MAC[i-8];
				frame[14] = 0;
				frame[15] = 50;
				alt_printf("Transmitting...\n");
			    alt_avalon_sgdma_construct_mem_to_stream_desc( &tx_descriptor, &tx_descriptor_end, (alt_u32 *)frame, 66, 0, 1, 1, 0);

			    // Set up non-blocking transfer of sgdma transmit descriptor
			    alt_avalon_sgdma_do_async_transfer( sgdma_tx_dev, &tx_descriptor );

				while (alt_avalon_sgdma_check_descriptor_status(&tx_descriptor) != 0);


			}
		}
		else{
			if (print != in) {
				IOWR_ALTERA_AVALON_PIO_DATA(LED_BASE, 0);
				alt_printf( "Switch off LED \n" );
			}
		}
	}

	return 0;
}

/****************************************************************************************
 * Subroutine to read incoming Ethernet frames
****************************************************************************************/

/*
void rx_ethernet_isr (void *context)
{

	//Include your code to show the values of the source and destination addresses of the received frame. For example:
	if(print){

int i;
	unsigned char sa[6] = {rx_frame[0], rx_frame[1], rx_frame[2], rx_frame[3], rx_frame[4], rx_frame[5] };
	unsigned char da[6] = {rx_frame[6], rx_frame[7], rx_frame[8], rx_frame[9], rx_frame[10], rx_frame[11] };
	int len = rx_frame[12] * 256 + rx_frame[13];

        char flag = 1;
	        for (i = 0; i < 6; i++){
            if (da[i] != MAC[i] && da[i] != 0xFF)
             flag = 0;
	          }

        if (len > 14 && flag) {

             if (rx_frame[14] == 0x4D && rx_frame[15] == 0x43 && rx_frame[16] == 0x50) {

               }
            else {
            flag = 0;
               }
 if (flag) {
        alt_printf( "Source address: %x:%x:%x:%x:%x:%x, Destination address: %x:%x:%x:%x:%x:%x\n", rx_frame[0], rx_frame[1],rx_frame[2],rx_frame[3],rx_frame[4],rx_frame[5],rx_frame[6],rx_frame[7],rx_frame[8],rx_frame[9],rx_frame[10],rx_frame[11]);

        alt_printf( "Length: %x:%x\n", rx_frame[12], rx_frame[13]);

        alt_printf( "MCP header: %x:%x:%x:%x:%x:%x:%x:%x:%x\n", rx_frame[14], rx_frame[15],rx_frame[16],rx_frame[17],rx_frame[18],rx_frame[19],rx_frame[20],rx_frame[21],rx_frame[22]);

           }

    }
	else {

		volatile int * tse = (int *) TSE_BASE;
		int nRx = *(tse + 0x1B);
		int nEr = *(tse + 0x1C) + *(tse + 0x1D);
		int nBx = *(tse + 0x2A);

		printf("Number of correctly rec'd frames: %u; Frames with errors: %u; Broadcast frames: %u\n", nRx, nEr, nBx);




	}




	// Wait until receive descriptor transfer is complete
	while (alt_avalon_sgdma_check_descriptor_status(&rx_descriptor) != 0)
		;

	// Create new receive sgdma descriptor
	alt_avalon_sgdma_construct_stream_to_mem_desc( &rx_descriptor, &rx_descriptor_end, (alt_u32 *)rx_frame, 0, 0 );

	// Set up non-blocking transfer of sgdma receive descriptor
	alt_avalon_sgdma_do_async_transfer( sgdma_rx_dev, &rx_descriptor );
}
*/

//TODO: implement this
void tx_ethernet(unsigned char *frame, unsigned char frame_len) {
	// Create sgdma transmit descriptor
    printf ("Its transmitting here\n");
	alt_avalon_sgdma_construct_mem_to_stream_desc( &tx_descriptor, &tx_descriptor_end, (alt_u32 *)frame, frame_len, 0, 1, 1, 0);

    // Set up non-blocking transfer of sgdma transmit descriptor
    alt_avalon_sgdma_do_async_transfer( sgdma_tx_dev, &tx_descriptor );

	while (alt_avalon_sgdma_check_descriptor_status(&tx_descriptor) != 0);

}


//Should we add active flag for each client and filter out msgs for inactive clients?
//Probably let the clients handle this
void parse_mcp_packet (unsigned char *packet, unsigned char *source_mac) {
	int i, j;
	unsigned char sa = packet[6];
	printf ("SA in Parse_MSP_packet %x\n", sa);
	unsigned char da = packet[7];
	printf ("DA in Parse_MSP_packet %x\n", da);
	int len = 256 * packet[4] + packet[5];
	unsigned char tx_buffer [256];
	//tx_buffer[0] = 0;
	//tx_buffer[1] = 0;

	switch(packet[8]) {
	case 0: //Standard ASCII message
		alt_printf("Rec'd standard ASCII message\n");
		if (da < num_clients) {
			//Construct MAC frame: SA, DA, frame len, M, C, P, 0, sa, da, 0, payload...
			for (i = 0; i < 6; i++) tx_buffer[i+2] = address_table[da][i];
			for (i = 6; i < 12; i++) tx_buffer[i+2] = MAC[i-6];
			tx_buffer[14] = (len+14) / 256;
			tx_buffer[15] = (len+14) % 256;
			tx_buffer[16] = 0x4D;
			tx_buffer[17] = 0x43;
			tx_buffer[18] = 0x50;
			tx_buffer[19] = 0;
            tx_buffer[20] = packet[4];// make new packet length the same as received packet length
            tx_buffer[21] = packet[5];
			tx_buffer[22] = sa;
			tx_buffer[23] = da;
			tx_buffer[24] = 0; //Type
			for (i = 0; i < len; i++) tx_buffer[i+25] = packet[i+9];
			tx_ethernet(tx_buffer, len+16+9);
		}
		//Else: bad client address, ignore


		break;
	case 1: //Req to join
		//1) Assign new MCP address based on num_clients. Send message to sending client with DA = new MCP addr (SA doesnt matter)
		//2) Put new client's MAC address into adress_table at location num_clients
		//3) Increment num_clients
		//4) Broadcast type 3 message with SA = new MCP address and payload = [New client's ascii name]
			alt_printf("Rec'd req to join\n");
            if (num_clients < 19)
            {
            //2
            da = num_clients;
            for(i = 0; i < 6; i++) address_table[num_clients][i] = source_mac[i];
            for(i = 0; i < len; i++) client_table[num_clients][i] = packet[i+9];
            packet[len] = 0;
            num_clients++;
            //1
			for (i = 0; i < 6; i++) tx_buffer[i+2] = address_table[da][i];
			for (i = 6; i < 12; i++) tx_buffer[i+2] = MAC[i-6];
			tx_buffer[14] = 0;
            tx_buffer[15] = 50;
            tx_buffer[16] = 0x4D;
            tx_buffer[17] = 0x43;
            tx_buffer[18] = 0x50;
            tx_buffer[19] = 0; //version
            tx_buffer[20] = 0; //length
            tx_buffer[21] = 0; //Length - just header, no payload
            tx_buffer[22] = 0; //Server is addr 0
            tx_buffer[23] = da; //New MCP
            tx_buffer[24] = 1; //Type...should type for confirmation message same as client joined or different?
            for (i = 25; i < 66; i++) tx_buffer[i] = 0;
            alt_printf("Transmitting response: %x:%x:%x:%x:%x:%x\n", tx_buffer[2],tx_buffer[3],tx_buffer[4],tx_buffer[5],tx_buffer[6],tx_buffer[7]);
            tx_ethernet(tx_buffer, 66);

            for (j = 1; j < num_clients; j++) {
				//Now broadcast about new client
				for(i = 0; i < 6; i++) tx_buffer[i+2] = 0xFF;

				tx_buffer[22] = j; //New client MCP address
				tx_buffer[23] = 0xFF;
				tx_buffer[24] = 3;
				i = 0;
				while (client_table[j][i] != 0) {
					tx_buffer[i+25] = client_table[j][i];
					i++;
				}
				tx_buffer[21] = i;
				tx_ethernet(tx_buffer, 66);
            }


            //4
            //for(i = 6; i <12; i++) tx_buffer[i] = 0xFF;
			//	tx_buffer[20] = da;
			//	tx_buffer[21] = 0xFF;
			//	tx_ethernet(tx_buffer, len);
            } //else mac routing table is full

		break;

	case 2: //Req to leave
		//Send type 4 message with MCP addr of leaving client as SA
			alt_printf("Rec'd request to leave\n");
            for (i = 0; i < 6; i++) tx_buffer[i+2] = MAC[i];
            for (i = 6; i < 12; i++) tx_buffer[i+2] = 0xFF;
            tx_buffer[14] = (len + 14) / 256;
            tx_buffer[15] = (len + 14) % 256;
            tx_buffer[16] = 0x4D;
            tx_buffer[17] = 0x43;
            tx_buffer[18] = 0x50;
            tx_buffer[19] = 0;
            tx_buffer[20] = packet[4];// make new packet length the same as received packet length
            tx_buffer[21] = packet[5];
            tx_buffer[22] = 0xFF;
            tx_buffer[23] = sa;
            tx_buffer[24] = 4;
            for (i = 9; i < len; i++) tx_buffer[i+15] = packet[i];//tx_buffer index 12 --> 14
            tx_ethernet(tx_buffer, len+14);


		break;


	}


}


void rx_ethernet_isr (void *context)
{
	int i;
	unsigned char da[6] = {rx_frame[2], rx_frame[3], rx_frame[4], rx_frame[5], rx_frame[6], rx_frame[7] };
	unsigned char sa[6] = {rx_frame[8], rx_frame[9], rx_frame[10], rx_frame[11], rx_frame[12], rx_frame[13] };
	printf("source address: %x:%x:%x:%x:%x:%x\n", rx_frame[8], rx_frame[9], rx_frame[10], rx_frame[11], rx_frame[12], rx_frame[13]);
	int len = rx_frame[14] * 256 + rx_frame[15];
	char flag = 1;
	for (i = 0; i < 6; i++){
		if (da[i] != MAC[i] && da[i] != 0xFF)
			flag = 0;
	}

	if (len > 14 && flag) {

		if (rx_frame[16] == 0x4D && rx_frame[17] == 0x43 && rx_frame[18] == 0x50) {

		}
		else {
			flag = 0;
		}

		if (flag) {
			alt_printf("Rec'd MCP frame\n");
			parse_mcp_packet(rx_frame + 16, sa); //also need sender's mac address for populating table


		}




	}


	// Wait until receive descriptor transfer is complete
	while (alt_avalon_sgdma_check_descriptor_status(&rx_descriptor) != 0)
		;

	// Create new receive sgdma descriptor
	alt_avalon_sgdma_construct_stream_to_mem_desc( &rx_descriptor, &rx_descriptor_end, (alt_u32 *)rx_frame, 0, 0 );

	// Set up non-blocking transfer of sgdma receive descriptor
	alt_avalon_sgdma_do_async_transfer( sgdma_rx_dev, &rx_descriptor );
}

