module Assignment3(  // module and port declaration: fill the gaps
	// Clock
	input clk_50,
	
	// KEY (reset)
	input   KEY, 
	
	//LED
	output LED,
	 
	//Switch
	input SWITCH,
	
	// Ethernet : the signals used are: the RGMII transmit clock, 
	//the MDC reference, the MDIO, the hardware reset,
   //	the RGMII receive clock, the receive data, the receive data valid, 
	//the transmit data and the transmit enable (check the manual)
	output  RGMII_CLK,
	output  ENET_MDC ,
	inout   ENET_MDIO,
	output  ENET_RST_N ,
	input   rx_clk ,
	input  [3:0] rgmii_in  ,
	input  rx_control ,
	output [3:0] rgmii_out ,
	output tx_control       
);

	wire sys_clk, clk_125, clk_25, clk_2p5, clk_125_90, clk_25_90, clk_2p5_90, tx_clk;
	wire core_reset_n;
	wire mdc, mdio_in, mdio_oen, mdio_out;
	wire eth_mode, ena_10;


	// Assign MDIO and MDC signals
	
	assign mdio_in   = ENET_MDIO;
	assign ENET_MDC  = mdc;
	assign ENET_MDIO = mdio_oen ? 1'bz : mdio_out;
	
	//Assign reset
	
	assign ENET_RST_N = core_reset_n;
	
	//PLL instance
	
	my_pll pll_inst(
		.areset	(~KEY),
		.inclk0	(clk_50),
		.c0		(sys_clk),
		.c1		(clk_125),
		.c2		(clk_25),
		.c3		(clk_2p5),
		.locked	(core_reset_n)
	); 
	
	mt_pll_x pll_x_inst(
	    .inclk0 (clk_50),
		 .c0     (clk_125_90),
		 .c1     (clk_25_90),
		 .c2      (clk_2p5_90)
		 );
	
	// Clock for transmission
	
	assign tx_clk = eth_mode ? clk_125 :       // GbE Mode   = 125MHz clock
	                ena_10 ? clk_2p5 : clk_25      // 10Mb Mode  = 2.5MHz clock
	                          ;         // 100Mb Mode = 25 MHz clock
									  
									  
	assign RGMII_CLK = eth_mode ? clk_125_90 : 
	                ena_10 ? clk_2p5_90 : clk_25_90 ;
	                          
	// ALTDDIO_OUT instance
	

	// Nios II system instance
	
    nios_system system_inst (
        .clk_clk (sys_clk),                                            					//  system clock (input)
        .reset_reset_n  (core_reset_n),                      				      			//  system reset (input)
	.led_export (LED),										// led (output)
	.switch_export (SWITCH),										// swicht button (input)
        .tse_pcs_mac_tx_clock_connection_clk 	(tx_clk), 			//  transmit clock (input)
        .tse_pcs_mac_rx_clock_connection_clk 	(rx_clk),		 		//  receive clock (input)
        .tse_mac_mdio_connection_mdc               (mdc),             		//  mdc (output)
        .tse_mac_mdio_connection_mdio_in         (mdio_in),           	//  mdio_in (input)
        .tse_mac_mdio_connection_mdio_out       (mdio_out),          	//  mdio_out (output)
        .tse_mac_mdio_connection_mdio_oen      (mdio_oen),     	     	//  mdio_oen (output)
        .tse_mac_rgmii_connection_rgmii_in         (rgmii_in),      			//  rgmii_in (rx data, input)
        .tse_mac_rgmii_connection_rgmii_out       (rgmii_out),	     			//  gmii_out (tx data, output)
        .tse_mac_rgmii_connection_rx_control      (rx_control),      			//  rx_control (receive data valid, input)
        .tse_mac_rgmii_connection_tx_control      (tx_control),      			//  tx_control (tx enable, output)
        .tse_mac_status_connection_eth_mode    (eth_mode),	                         //  eth_mode (output)
        .tse_mac_status_connection_ena_10        (ena_10),          	                //   ena_10	  (output)
    );	
    
    

endmodule 