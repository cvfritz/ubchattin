import thread
from EthWrapper import * #tx_frame, rx_frame, eth_init, NEW_FRAME
import EthWrapper   

NEW_MSG = False
NEW_USR = False
message = ''
message_sender = ''
new_user_name = ''
client_table = {} #keys = client names, values = MCP addresses
M = 0x4D
C = 0x43
P = 0x50
SA = 0

new_user_list = []

def connectToServer(myName):
    global SA
    
    stat = eth_init()
    if stat:
        f = [M,C,P,0,0,0,0xFF,0xFF,1] #Request to join
        for ch in myName:
            f.append(ord(ch))
        f[5] = len(f)
        tx_frame(f)
        while True:
            while not EthWrapper.NEW_FRAME:
                pass
            fr = rx_frame()
            if fr[6] == 0 and fr[8] == 1: #Response from server
                SA = fr[7]
                print "Assigned MCP address", SA
                break

        thread.start_new_thread(receiveMessage, ())
    return stat

def sendMessage(msg, clientID):
    print "Sending to", clientID
    #Create MCP message, and send to EthWrapper
    frame = [M,C,P,0] #Magic number and version
    #Will insert length later
    frame.append(SA)
    frame.append(client_table[clientID]) #DA
    frame.append(0) #ASCII message
    for ch in msg:
        frame.append(ord(ch)) #ASCII code of each character 
    frame.insert(4, len(msg) / 256)
    frame.insert(5, len(msg) % 256)
    tx_frame(frame)

def receiveMessage():
    global NEW_MSG
    global NEW_USR
    global message
    global message_sender
    global new_user_name
    
    while True:
        if EthWrapper.NEW_FRAME:
            frame = rx_frame()
            if frame[:3] == [77,67,80]: #Check for MCP magic number
                msg_len = frame[4]*256 + frame[5]
                #Different cases for each MCP message type
                if frame[8] == 0:
                    #ASCII message
                    print "Got ascii message"
                    message = ''
                    client_table_rev = {v:k for k,v in client_table.items()}
                    message_sender = client_table_rev[frame[6]] #Reverse lookup by SA to get client name 
                    for ch in frame[9:msg_len+9]:
                        message += chr(ch)
                    NEW_MSG = True
                    print "Got message from", message_sender, message
                    
                elif frame[8] == 3:
                    #New client
                    newID = frame[6]
                    if newID != SA: #Don't add new user for yourself!
                        new_user_name = ''
                        for ch in frame[9:msg_len+9]:
                            new_user_name += chr(ch)
                        
                        if new_user_name not in client_table:
                            print "New client ID", new_user_name
                            client_table[new_user_name] = newID
                            #NEW_USR = True
                            new_user_list.append(new_user_name)
                        
                        


def readMessage():
    global NEW_MSG
    NEW_MSG = False
    return message, message_sender

def getNewUser():
    global NEW_USR
    NEW_USR = False
    return new_user_name









    




