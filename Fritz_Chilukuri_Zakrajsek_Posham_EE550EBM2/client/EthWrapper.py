from winpcapy import *
import fileinput
import thread
NEW_FRAME = False
frame = []
import time
SA = []
DA = [0xFF]*6

fp = None                     
alldevs=POINTER(pcap_if_t)()  
header=POINTER(pcap_pkthdr)()   # incoming header
packetData=POINTER(c_ubyte)()     # incoming packet  

def getInterfaces():
    import ctypes.wintypes

    MAX_ADAPTER_NAME_LENGTH = 256
    MAX_ADAPTER_DESCRIPTION_LENGTH = 128
    MAX_ADAPTER_ADDRESS_LENGTH = 6

    class IP_ADDR_STRING(ctypes.Structure):
        pass
    LP_IP_ADDR_STRING = ctypes.POINTER(IP_ADDR_STRING)
    IP_ADDR_STRING._fields_ = [
        ("next", LP_IP_ADDR_STRING),
        ("ipAddress", ctypes.c_char * 16),
        ("ipMask", ctypes.c_char * 16),
        ("context", ctypes.wintypes.DWORD)]

    class IP_ADAPTER_INFO (ctypes.Structure):
        pass
    LP_IP_ADAPTER_INFO = ctypes.POINTER(IP_ADAPTER_INFO)
    IP_ADAPTER_INFO._fields_ = [
        ("next", LP_IP_ADAPTER_INFO),
        ("comboIndex", ctypes.wintypes.DWORD),
        ("adapterName", ctypes.c_char * (MAX_ADAPTER_NAME_LENGTH + 4)),
        ("description", ctypes.c_char * (MAX_ADAPTER_DESCRIPTION_LENGTH + 4)),
        ("addressLength", ctypes.c_uint),
        ("address", ctypes.c_ubyte * MAX_ADAPTER_ADDRESS_LENGTH),
        ("index", ctypes.wintypes.DWORD),
        ("type", ctypes.c_uint),
        ("dhcpEnabled", ctypes.c_uint),
        ("currentIpAddress", LP_IP_ADDR_STRING),
        ("ipAddressList", IP_ADDR_STRING),
        ("gatewayList", IP_ADDR_STRING),
        ("dhcpServer", IP_ADDR_STRING),
        ("haveWins", ctypes.c_uint),
        ("primaryWinsServer", IP_ADDR_STRING),
        ("secondaryWinsServer", IP_ADDR_STRING),
        ("leaseObtained", ctypes.c_ulong),
        ("leaseExpires", ctypes.c_ulong)]

    GetAdaptersInfo = ctypes.windll.iphlpapi.GetAdaptersInfo
    GetAdaptersInfo.restype = ctypes.wintypes.DWORD
    GetAdaptersInfo.argtypes = [LP_IP_ADAPTER_INFO, ctypes.POINTER(ctypes.c_ulong)]

    adapters = (IP_ADAPTER_INFO * 10)()
    buflen = ctypes.c_ulong(ctypes.sizeof(adapters))
    GetAdaptersInfo(ctypes.byref(adapters[0]), ctypes.byref(buflen))

    ifaces = {}
    index = 0
    while index in range(0, 10):
        a = adapters[index]
        iface = {}
        iface['desc'] = a.description
        iface['mac'] = [int(part) for part in a.address]
        while len(iface['mac']) > 6: iface['mac'] = iface['mac'][:-1]
        if("WiFi" in a.description or "Wireless" in a.description or "Bluetooth" in a.description or "Check Point" in a.description or
           "Centrino" in a.description or "Virtual" in a.description or "USB2.0" in a.description or
           (iface['mac'] == [0, 0, 0, 0, 0, 0])):
            iface['use'] = False
        else:
            iface['use'] = True                             
        print ("adapter:", a, " Interface:", iface)
        ifaces[a.adapterName] = iface
        if(iface['use']):
            break
        index += 1
    return ifaces 

def ethInit():
    global fp
    global SA
    my_interfaces_dict = {}

    if fp is not None:
        pcap_close(fp)

    ##Local buffers
    pcap_errbuf = create_string_buffer(PCAP_ERRBUF_SIZE)  
    my_interfaces_dict = getInterfaces()

    #CVF 11/14/14 - if no usable devices found, try to enable "local area connection"
    if not any(my_interfaces_dict[k]["use"] for k in my_interfaces_dict):
        os.system("netsh interface set interface name=\"Local Area Connection\" admin=enabled")
        my_interfaces_dict = getInterfaces()   
        time.sleep(2)


    # alldevs is a global pointer to a pcap_if_t
    if (pcap_findalldevs(byref(alldevs), pcap_errbuf) == -1):
        print ("MTMPEth.ethInit> No interfaces found")
        return False
    ##Begin connection. 
    inum = 0
    i = 0
    saved_dev = None
    devname = None
    available_interface = alldevs.contents

    while available_interface:
        inum = inum + 1
        devname = '{' + available_interface.name.split('{')[1]
        if(devname not in my_interfaces_dict):
            print "MTMPEth.ethInit> pcap_findalldevs interface not in getAdapters dictionary: "
        else:
            if(my_interfaces_dict[devname]['use'] == True):
                print "ethInit> Available Wired Interface #:", inum, " devname:", devname, " description:", available_interface.description, " name:", available_interface.name
                SA = my_interfaces_dict[devname]['mac']
                print "ethInit> MAC address: " + str(SA)
                break;         
            else:
                print "MTMPEth.ethInit> tossing interface that isn't wired #:", i, " description:", available_interface.description, " name:", available_interface.name            

        ## if we are here, we didn't find a non-wireless interface YET... so keep indexing through   
        if (available_interface.next):
            available_interface = available_interface.next.contents
        else:
            available_interface = False          

    if (inum == 0):
        print "MTMPEth.ethInit> No interfaces found"
        return False
        ## return above prevents us getting below with devname None

    available_interface = alldevs 
    for i in range(0,inum-1):
        available_interface=available_interface.contents.next 
    
    ## trying to reduce the read timeout from 1000ms to 10ms so that we don't sit on packets for 1s
    fp = pcap_open_live(available_interface.contents.name,65536,1,10,pcap_errbuf)
    if(fp is None):
        print "MTMPEth.ethInit> pcap_open_live failed to open device ", available_interface.contents.name, saved_dev.description, " pcap error msg:", pcap_errbuf
        return False

    print "MTMPEth.ethInit> Ethernet initialized"
    return True
    
frame_stack = []

def eth_init():
    stat = ethInit()
    if stat:
        thread.start_new_thread(updateThread, ())
    return stat

def tx_frame(fr):
    while len(fr) < 64 : fr.append(0)
    fr_len = len(fr)
    tx_fr = DA + SA + [0,0] + fr
    tx_fr[12] = fr_len / 256
    tx_fr[13] = fr_len % 256

    frame_c = (c_ubyte * (len(tx_fr)))()             
    for i in range(len(tx_fr)):                  
        frame_c[i] = tx_fr[i]
    pcap_return = pcap_sendpacket(fp,frame_c,len(tx_fr))

def rx_frame():
    global frame_stack
    global NEW_FRAME
    ret = frame_stack[0]
    if len(frame_stack) > 1: 
        frame_stack = frame_stack[1:]
    else:
        NEW_FRAME = False
        frame_stack = []
    return ret
    

def updateThread():
    global frame, NEW_FRAME, DA, frame_stack
    while True:
        pcapNextStatus = pcap_next_ex(fp, byref(header), byref(packetData))
        if pcapNextStatus > 0:
            if(packetData[0:6] == SA or packetData[0:6] == [0xFF]*6):
                frame = packetData[14:header.contents.len]
                frame_stack.append(frame)
                NEW_FRAME = True
                if packetData[14:17] == [0x4D, 0x43, 0x50]:
                    DA = packetData[6:12]

