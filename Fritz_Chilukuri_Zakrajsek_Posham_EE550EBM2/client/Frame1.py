#Boa:Frame:Frame1

import wx
import os
import glob
import thread
import GUI2Eth
import thread
import wx.lib.newevent
import time

class TabPanel(wx.Panel):
    def __init__(self, parent, user):
        wx.Panel.__init__(self, parent=parent, id=wx.ID_ANY)
        sizer = wx.BoxSizer(wx.VERTICAL)
        self.txtct = wx.TextCtrl(self, wx.ID_ANY, "", size=wx.Size(312,200), style=wx.TE_MULTILINE)
        self.txtct.SetEditable(False)
        sizer.Add(self.txtct, 0, wx.ALL, 5)
        self.SetSizer(sizer)
        self.user = user
    

def create(parent):
    return Frame1(parent)

[wxID_FRAME1, wxID_FRAME1CONNECTBUTTON, wxID_FRAME1NOTEBOOK1, 
 wxID_FRAME1SENDBUTTON, wxID_FRAME1TEXTCTRL1, 
] = [wx.NewId() for _init_ctrls in range(5)]

NewUserEvent, EVT_NEW_USER_EVENT = wx.lib.newevent.NewEvent()
NewMsgEvent, EVT_NEW_MSG_EVENT = wx.lib.newevent.NewEvent()

class Frame1(wx.Frame):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_FRAME1, name='', parent=prnt,
              pos=wx.Point(68, 97), size=wx.Size(407, 498),
              style=wx.DEFAULT_FRAME_STYLE,
              title=u"UBChattin' v1.0.0 (EE550EBM2 Final Project)")
        self.SetClientSize(wx.Size(407, 476))

        self.notebook1 = wx.Notebook(id=wxID_FRAME1NOTEBOOK1, name='notebook1',
              parent=self, pos=wx.Point(32, 16), size=wx.Size(344, 296),
              style=0)

        self.textCtrl1 = wx.TextCtrl(id=wxID_FRAME1TEXTCTRL1, name='textCtrl1',
              parent=self, pos=wx.Point(48, 344), size=wx.Size(312, 88),
              style=wx.TE_MULTILINE, value='')

        self.connectButton = wx.Button(id=wxID_FRAME1CONNECTBUTTON,
              label=u'Connect', name=u'connectButton', parent=self,
              pos=wx.Point(78, 436), size=wx.Size(84, 20), style=0)
        self.connectButton.Bind(wx.EVT_BUTTON, self.OnConnectButtonButton,
              id=wxID_FRAME1CONNECTBUTTON)

        self.sendButton = wx.Button(id=wxID_FRAME1SENDBUTTON, label=u'Send',
              name=u'sendButton', parent=self, pos=wx.Point(220, 436),
              size=wx.Size(84, 20), style=0)
        self.sendButton.Bind(wx.EVT_BUTTON, self.OnSendButtonButton,
              id=wxID_FRAME1SENDBUTTON)
        self.sendButton.Disable()

        self.myName = wx.TextCtrl(id=wx.ID_ANY, name='myName',
              parent=self, pos=wx.Point(125, 315), 
              style=0, value='')

        self.lab1 = wx.StaticText(id=wx.ID_ANY, name='lab1', parent=self, 
              pos=wx.Point(48, 315), label='My Name:')
        self.parent = prnt

    def __init__(self, parent):
        self._init_ctrls(parent)
        thread.start_new_thread(self.updateThread, ())
        self.Bind(EVT_NEW_USER_EVENT, self.OnNewUserAdded)
        self.Bind(EVT_NEW_MSG_EVENT, self.OnNewMsg)
        self.textCtrl1.Bind(wx.EVT_TEXT_ENTER, self.OnSendButtonButton)
        self.textCtrl1.Bind(wx.EVT_CHAR, self.OnNewLine)
        self.userPages = []
        self.Show()

    def OnConnectButtonButton(self, event):
        self.myNameStr = self.myName.GetValue()
        if self.myNameStr == '':
            wx.MessageBox('Please enter your name first', 'Error', wx.OK)
            return
        stat = GUI2Eth.connectToServer(self.myNameStr)
        if stat:
            self.myName.SetEditable(False)
            self.sendButton.Enable()
        else:
            wx.MessageBox("Error connecting to server. Check network.", "Network error", wx.OK)

    def OnSendButtonButton(self, event):
        msg = self.textCtrl1.GetValue()
        GUI2Eth.sendMessage(msg, self.notebook1.GetCurrentPage().user)
        self.textCtrl1.SetValue('')
        self.textCtrl1.SetInsertionPoint(0)
        txtct = self.notebook1.GetCurrentPage().txtct
        txtct.SetValue(txtct.GetValue() + '\n' + self.myNameStr + '> ' + msg)
    
    def OnNewLine(self,event):
        if event.GetKeyCode() == 13:
            self.OnSendButtonButton(None)
        else:
            event.Skip()
        
        
    
    def OnNewUserAdded(self, evt):
        newUser = self.newUser #GUI2Eth.getNewUser()
        self.notebook1.AddPage ( TabPanel(self.notebook1, newUser), newUser)
        self.userPages.append(newUser)
        self.textCtrl1.SetFocus()
        
    
    def OnNewMsg(self, event):
        message, message_sender = GUI2Eth.readMessage()
        self.notebook1.ChangeSelection(self.userPages.index(message_sender))
        txtct = self.notebook1.GetCurrentPage().txtct
        txtct.SetValue(txtct.GetValue() + '\n' + message_sender + '> ' + message)
       
    
    def updateThread(self):
        while True:
            if len(GUI2Eth.new_user_list) > 0:
                self.newUser = GUI2Eth.new_user_list[0]
                if (GUI2Eth.new_user_list) > 1:
                    GUI2Eth.new_user_list = GUI2Eth.new_user_list[1:]
                else:
                    GUI2Eth.new_user_list = []
                evt = NewUserEvent()
                wx.PostEvent(self, evt)
                time.sleep(1)
            if GUI2Eth.NEW_MSG:
                evt = NewMsgEvent()
                wx.PostEvent(self, evt)
                time.sleep(1)
            
                


        



